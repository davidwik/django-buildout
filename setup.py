from setuptools import setup, find_packages

setup(
    name = "website",
    version = "1.0",
    url = 'http://localhost',
    license = 'BSD',
    description = "A website",
    author = 'David Viktorsson',
    packages = find_packages('src'),
    package_dir = {'': 'src'},
    install_requires = ['setuptools', 'Django'],
)
