# A simple Django buildout #
A basic django buildout. With iPython and ipdb.

### Setup details ###

Run the following commands to set it up:
```
#!bash
> virtualenv -p python3 .
> source bin/activate
> python bootstrap.py
> buildout
```